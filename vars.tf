

variable "aws_region" {
  type        = string
  description = "AWS region to deploy to"
  default     = "eu-central-1"
}
